const integrantes = [
    { nombre: "Ruben ", apellido:"Ocampos",matricula:"Y12700", rol: "Desarrollador",codigo: "06",url:"Y12700/index.html" },
    { nombre: "Juan ", apellido:"Aquino",matricula:"Y19937", rol: "Desarrollador",codigo: "01",url:"Y19937/index.html" },
    { nombre: "Junior  ", apellido:"Cabral",matricula:"Y25387", rol: "Desarrollador",codigo: "02",url:"Y25387/index.html" },
    { nombre: "Sebastian ",apellido:"Duarte",matricula:"Y25495", rol: "Desarrollador",codigo: "03",url:"Y25495/index.html" },
    { nombre: "Elvio ", apellido:"Aguero",matricula:"Y19099", rol: "Desarrollador",codigo: "04",url:"Y19099/index.html" },
    { nombre: "Luis ", apellido:"Delgado",matricula:"UG0085", rol: "Desarrollador",codigo: "05",url:"UG0085/index.html" },
]
const tiposMedio = [
    {tipo :"Youtube", descripcion : "Videos en linea"},
    {tipo :"Twitter/X", descripcion : "Mnesajes cortos y multimedia"},
    {tipo :"Dibujo", descripcion : "Ilustraciones y graficos"},
]
const media = [
    {// Media de Ruben
        url:"Y12700/index.html",
        nombre: "Ruben",
        apellido:"Ocampos",
        matricula:"Y12700",
        titulo: "Mi Video Favorito en YouTube.",
        parrafo:"Un buen video explicando el tema de los hackers.",
        imagen:"",
        alt:""
    },
    {
        url:"Y12700/index.html",
        matricula:"Y12700",
        video:"https://www.youtube.com/watch?v=UVQrM86zc30",
        titulo:"Imagen Favorita",
        parrafo:"Esta imagen representa mi color favorito.",
        imagen:"/assets/Ruben imagen.jpg",
        video:"",
        alt:"Video de youtube"
        
    },

    {// Media de Juan
        url:"Y19937/index.html",
        nombre: "Juan",
        apellido:"Aquino",
        matricula:"Y19937",
        video:"https://www.youtube.com/embed/_Yhyp-_hX2s",
        titulo: "Mi Video Favorito en YouTube.",
        parrafo:"Es mi video favorito por el mensaje que nos deja. En 'Lose Yourself', Eminem nos motiva a superar nuestras dudas y limitaciones para alcanzar nuestros objetivos.La canción nos recuerda que solo tenemos una oportunidad en la vida y que no debemos dejarla pasar.La canción también habla sobre la importancia de la perseverancia y la determinación. No importa cuántas veces te caigas,lo importante es que te levantes y sigas adelante.",
        imagen:"",

        alt:""
    },
    {
        url:"Y19937/index.html",
        matricula:"Y19937",
        video:"https://www.youtube.com/embed/_Yhyp-_hX2s",
        titulo:"Imagen Favorita",
        parrafo:"Esta imagen captura dos de mis pasiones en un solo momento: el deporte y la belleza del atardecer.Esta foto representa la convergencia de dos aspectos importantes de mi vida: mi pasión por el deporte y mi aprecio por la belleza de la naturaleza al atardecer. Cada vez que la veo, me inspira a seguir persiguiendo mis sueños con determinación y a encontrar alegría en los pequeños placeres que la vida tiene para ofrecer.",
        imagen:"/assets/Juan-foto.jpeg",
        video:"",
        alt:"Video de youtube"
        
    },
    {
        url:"Y19937/index.html",
        matricula:"Y19937",
        titulo: "Dibujo Propio",
        parrafo: "Este dibujo representa lo que me gusta, el atardecer, la música, el fútbol y el baloncesto. Cada elemento representa una parte importante de mi vida,desde belleza del atardecer hasta la emoción del deporte y la inspiración de la música.",
        imagen:"/assets/Juan-Dibujo.jpg",
        video:"",
        alt:"Foto dibujo de JUan "
    },
    {
        url:"Y19937/index.html",
        matricula:"Y19937",
        titulo: "TW",
        parrafo: "Un Twitt de la pagina X",
        imagen:"",
        video:"",
        alt:"",
        html: '<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Most people don&#39;t tag their precise location in Tweets, so we&#39;re removing this ability to simplify your Tweeting experience. You&#39;ll still be able to tag your precise location in Tweets through our updated camera. It&#39;s helpful when sharing on-the-ground moments.</p>&mdash; Support (@Support) <a href="https://twitter.com/Support/status/1141039841993355264?ref_src=twsrc%5Etfw">June 18, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>'
    },

    //-------------------------------------------------------------------------------------------------------------------------------
    {//Media de Junior
        url:"Y25387/index.html",
        matricula:"Y25387",
        nombre: "Junior",
        apellido:"Cabral",
        video:"https://www.youtube.com/embed/Tsnyq-3k7Bg?si=-Bzir8axv4WRU4MS&amp;controls=0",
        titulo: "Mi Video Favorito en YouTube.",
        parrafo:"Uno de los vídeos más interesantes para mí trata sobre la idea preconcebida de que estamos hechos de partículas, sino de algo más, tal vez campos que vibran en el espacio y en base a esas fluctuaciones existimos.",
        imagen:"",

        alt:""
    },
    {
        url:"Y25387/index.html",
        matricula:"Y25387",
        video:"",
        titulo:"Imagen Favorita",
        parrafo:"Una de las cosas que más me gusta hacer es observar el atardecer. Es un momento de paz y tranquilidad que me ayuda a relajarme y pensar en las cosas que me importan, nunca dejando de cuestionarme los hechos físicos que llevan a que se puedan observar, como en esta imagen",
        imagen:"../../assets/Junior%20-%20Foto.jpg",
        video:"",
        alt:"Imagen favorita"
        
    },
    {
        url:"Y25387/index.html",
        matricula:"Y25387",
        titulo: "Dibujo Propio",
        parrafo: "Algo que me apasiona bastante, además de la física de partículas y los atardeceres, es poder mirar más allá de donde estamos, imaginar los mundos posibles en la vastedad del cosmos y lo impresionante que son, como este dibujo de un planeta con un sistema de anillos parecido al de Júpiter.",
        imagen:"../../assets/Junior-Dibujo.png",
        video:"",
        alt:"Foto dibujo de Junior "
    },
    {
        url:"Y25387/index.html",
        matricula:"Y25387",
        titulo: "TW",
        parrafo: "Un Twitt de la pagina X",
        imagen:"",
            video:"",
            alt:"",
            html: '<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Most people don&#39;t tag their precise location in Tweets, so we&#39;re removing this ability to simplify your Tweeting experience. You&#39;ll still be able to tag your precise location in Tweets through our updated camera. It&#39;s helpful when sharing on-the-ground moments.</p>&mdash; Support (@Support) <a href="https://twitter.com/Support/status/1141039841993355264?ref_src=twsrc%5Etfw">June 18, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>'
        },
    //--------------------------------
    

    {//Media de Sebastian
        url:"Y25495/index.html",
        matricula:"Y25495",
        nombre: "Sebastian",
        apellido:"Duarte",
        video:"https://www.youtube.com/embed/FRn6xXXF-7s?si=yVJP3egE0MB4siuE",
        titulo: "Mi Video Favorito en YouTube.",
        parrafo:"Mi video favorito es el trailer de la ultinma temporada de mi serie favorita, considerada una obra maestra narrativa y visual, para mi, simboliza el fin de una etapa en mi vida, ya que segui esta serie desde hace 8 años",
        imagen:"",

        alt:""
    },
    {
        url:"Y25495/index.html",
        matricula:"Y25495",
        video:"",
        titulo:"Imagen Favorita",
        parrafo:"Mi imagen favorita es el protagonista del videojuego dark souls, y la imagen representa para mi su desesperanza y su impotencia, pero al mismo tiempo el objetivo que lo mantiene el su travesía. Me gusta por todo lo que representa para mi y todo lo significó para mi el juego y su historia bien elaborada",
        imagen:"../../assets/Sebastian-foto.webp",
        video:"",
        alt:"Imagen favorita de Sebastian"

    },
    {
        url:"Y25495/index.html",
        matricula:"Y25495",
        titulo: "Dibujo Propio",
        parrafo: "Mi dibujo siento que representa la perseverancia, ya que esta fogata, es aquella en la que los aventureros descansan y se arman de valor para seguir con su viaje, tambien está sacada del videojuego dark souls",
        imagen:"../../assets/Sebastian-Dibujo.jpg",
        video:"",
        alt:"Dinujo de Sebastian "
    },
    {
        url:"Y25495/index.html",
        matricula:"Y25495",
        titulo: "TW",
        parrafo: "Un Twitt de la pagina X",
        imagen:"",
        video:"",
        alt:"",
        html: '<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Most people don&#39;t tag their precise location in Tweets, so we&#39;re removing this ability to simplify your Tweeting experience. You&#39;ll still be able to tag your precise location in Tweets through our updated camera. It&#39;s helpful when sharing on-the-ground moments.</p>&mdash; Support (@Support) <a href="https://twitter.com/Support/status/1141039841993355264?ref_src=twsrc%5Etfw">June 18, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>'
    },

    //-----------------------------------
    {//Media de Elvio
        matricula:"Y19099",
        nombre: "Elvio",
        apellido:"Aguero",
        video:"https://www.youtube.com/embed/NynNdkY2gx0?si=nzQjEF5y6hY6uSOp",
        titulo: "Mi Video Favorito en YouTube.",
        imagen:"",

        alt:""
    },
    {
        url:"Y19099/index.html",
        matricula:"Y19099",
        video:"",
        titulo:"Imagen Favorita",
        parrafo:"",
        imagen:"../../assets/Elvio-Foto.jpg",
        video:"",
        alt:"Imagen representativa"

    },
    {
        url:"Y19099/index.html",
        matricula:"Y19099",
        titulo: "Dibujo Propio",
        parrafo: "",
        imagen:"../../assets/Elvio-Dibujo.png",
        video:"",
        alt:"Foto dibujo de Elvio "
    },
    {
        url:"Y19099/index.html",
        matricula:"Y19099",
        titulo: "TW",
        parrafo: "Un Twitt de la pagina X",
        imagen:"",
        video:"",
        alt:"",
        html: '<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Most people don&#39;t tag their precise location in Tweets, so we&#39;re removing this ability to simplify your Tweeting experience. You&#39;ll still be able to tag your precise location in Tweets through our updated camera. It&#39;s helpful when sharing on-the-ground moments.</p>&mdash; Support (@Support) <a href="https://twitter.com/Support/status/1141039841993355264?ref_src=twsrc%5Etfw">June 18, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>'
    },
    //-----------------------------------
    {//Media de Luis
        url:"UG0085/index.html",
        matricula:"UG0085",
        nombre: "Luis",
        apellido:"Delgado",
        video:"https://www.youtube.com/embed/Hv5ET7azgEk?si=WxryTH3oIMGTTD-Z",
        titulo: "Mi Video Favorito en YouTube.",
        parrafo:"Uno de los vídeos que más me llama la atención es acerca de un libro que se llama La Vida Secreta De La Mente.",
        imagen:"",

        alt:""
    },
    {
        url:"UG0085/index.html",
        matricula:"UG0085",
        video:"",
        titulo:"Imagen Favorita",
        parrafo:"Una de las cosas que más me gusta es escalar cerros y observar los hermosos paisajes de nuestro pais. Cerro Hu",
        imagen:"../../assets/Luis Delgado.jpeg",
        video:"",
        alt:"Video de youtube"

    },
    {
        url:"UG0085/index.html",
        matricula:"UG0085",
        titulo: "Dibujo Propio",
        parrafo: "Siempre es importante aprender algo nuevo.",
        imagen:"../../assets/Luis Delgado-2.png",
        video:"",
        alt:"Foto dibujo de Luis "
    },
    {
        url:"UG0085/index.html",
        matricula:"UG0085",
        titulo: "TW",
        parrafo: "Un Twitt de la pagina X",
        imagen:"",
        video:"",
        alt:"",
        html: '<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Most people don&#39;t tag their precise location in Tweets, so we&#39;re removing this ability to simplify your Tweeting experience. You&#39;ll still be able to tag your precise location in Tweets through our updated camera. It&#39;s helpful when sharing on-the-ground moments.</p>&mdash; Support (@Support) <a href="https://twitter.com/Support/status/1141039841993355264?ref_src=twsrc%5Etfw">June 18, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>'
    },
//----------------------------------------------------------------
//home dinamico
{
    url:"/",
    matricula:"Home",
    titulo: "Pagina Grupo 01",
    titulo2:"Nombres y Matricula",
    logo: "Logo Del grupo",
    imagen:"./assets/logo.jpeg",
    video:"",
    alt:"Logo de grupo",
    html: ''
},

 ];
//exportar data.js
exports.integrantes = integrantes;
exports.tiposMedio = tiposMedio;
exports.media = media;
