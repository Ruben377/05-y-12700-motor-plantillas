const express = require('express');
const hbs = require('hbs');
const app = express();
require('dotenv').config({ path: '.env' });
const bodyParser = require("body-parser");

// importar archivo de rutas
const router = require('./routes/public');
const routerAdmin = require('./routes/admin');

// Definir y registrar el helper extractYouTubeID
hbs.registerHelper('extractYouTubeID', function(url) {
  const regex = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/;
  const match = url.match(regex);
  return match ? match[1] : null;
});

// Configuración de middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/", router);
app.use("/admin", routerAdmin);

// Creación de aplicación express
app.use(express.static('public'));
app.use("/admin", express.static('admin'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

hbs.registerPartials(__dirname + "/views/partials");

// Iniciar el servidor
const puerto = process.env.PORT || 3000;
app.listen(puerto, () => {
    console.log(`El servidor se está ejecutando en el puerto ${puerto}`);
});
