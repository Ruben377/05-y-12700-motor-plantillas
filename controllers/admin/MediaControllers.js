const express = require('express');
const { getALL, db } = require('../../db/conexion');
const path = require('path');
const fs = require('fs');

function convertToEmbedURL(url) {
    console.log("youtube:", url);
    if (!url) {
        return null;
    }

    const regex = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/;
    const match = url.match(regex);
    if (match && match[1]) {
        return `https://www.youtube.com/embed/${match[1]}`;
    }
    return null;
}

const MediaController = {
    index: async function (req, res) {
        try {
            const mediaItems = await getALL(`SELECT * FROM Media WHERE esta_borrado = 0`);
            const message = req.query.message || null;  // Obtener el mensaje de la query

            if (mediaItems.length === 0) {
                return res.render("admin/Media/index", { message: "No hay elementos de media", mediaItems: [] });
            }

            const processedMediaItems = mediaItems.map(item => ({
                ...item,
                esta_borrado: item.esta_borrado ? 'Inactivo' : 'Activo'
            }));

            res.render("admin/Media/index", { mediaItems: processedMediaItems, message });
        } catch (error) {
            console.error(error);
            return res.status(500).render("error");
        }
    },

    create: async function (req, res) { 
        const message = req.query.message;
        const datosForm = req.query;
        try {
            const integrantesActivos = await getALL(`SELECT * FROM Integrante WHERE esta_borrado = 0`);
            const tiposMedia = await getALL(`SELECT * FROM TipoMedio`);
            res.render('admin/Media/crearForm', {
                message,
                datosForm,
                integrantesActivos,
                tiposMedia,
            });
        } catch (error) {
            console.error('Error al obtener datos de la base de datos:', error);
            res.status(500).send('Error al obtener datos de la base de datos');
        }
    },

    store: async function (req, res) { 
        try {
            const { matricula, url, titulo, parrafo, video, tipo_medio, esta_borrado } = req.body;
            const imagen = req.file ? `/assets/${req.file.filename}` : null;

            if (!matricula || !url || !titulo || !parrafo || !tipo_medio) {
                return res.redirect('/admin/Media/crear?message=Todos los campos obligatorios deben ser proporcionados.');
            }

            const embedVideoURL = convertToEmbedURL(video);

            const maxOrdenResult = await new Promise((resolve, reject) => {
                db.get("SELECT MAX(orden) as maxOrden FROM Media", (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });

            const newOrden = (maxOrdenResult.maxOrden || 0) + 1;

            db.run(
                "INSERT INTO Media (matricula, url, titulo, parrafo, imagen, video, tipo_medio, esta_borrado, orden) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                [matricula, url, titulo, parrafo, imagen, embedVideoURL, tipo_medio, esta_borrado, newOrden],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.redirect('/admin/Media/crear?message=Error al insertar en la base de datos');
                    }
                    res.redirect("/admin/Media/crear?message=Elemento de media creado exitosamente");
                }
            );
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.redirect('/admin/Media/crear?message=Error al insertar en la base de datos');
        }
    },
    
    edit: function (req, res) {
        const idMedia = req.params.idMedia;

        db.get("SELECT m.*, i.nombre, i.apellido FROM Media m INNER JOIN Integrante i ON m.matricula = i.matricula WHERE m.id = ?", [idMedia], (err, Media) => {
            if (err) {
                console.error("Error al obtener el medio:", err);
                return res.status(500).render("error");
            }
    
            if (!Media) {
                return res.redirect('/admin/Media/listar?message=No se encontró el medio');
            }
    
            res.render("admin/Media/editForm", { Media });
        });
    },

    update: function (req, res) {
        const idMedia = req.params.idMedia;
        const { titulo, parrafo, video, esta_borrado } = req.body;

        if (!titulo || !parrafo || typeof esta_borrado === 'undefined') {
            console.error("Campos obligatorios faltantes:", { titulo, parrafo, esta_borrado });
            return res.redirect(`/admin/Media/edit/${idMedia}?message=Campos obligatorios faltantes`);
        }

        let imagen = req.body.imagen; // Obtener la imagen actual de la base de datos o formulario

        if (req.file) { // Si se ha enviado un nuevo archivo de imagen
            imagen = `/assets/${req.file.filename}`; // Actualizar la ruta de la imagen
        }

        let embedVideoURL = null;
        if (video) { // Solo convertir la URL del video si hay un dato en el campo de video
            embedVideoURL = convertToEmbedURL(video);
        }

        const sql = `
            UPDATE Media
            SET titulo = ?, parrafo = ?, imagen = ?, video = ?, esta_borrado = ?
            WHERE id = ?
        `;
        const params = [titulo, parrafo, imagen, embedVideoURL, esta_borrado, idMedia];

        db.run(sql, params, function (err) {
            if (err) {
                console.error("Error al actualizar el medio:", err);
                return res.redirect(`/admin/Media/edit/${idMedia}?message=Error al actualizar el medio`);
            }
            console.log(`Medio con ID ${idMedia} actualizado.`);
            res.redirect('/admin/Media/listar?message=Medio actualizado exitosamente');
        });
    },

    destroy: function (req, res) {
        const idMedia = req.params.idMedia;
        db.run("UPDATE Media SET esta_borrado = 1 WHERE id = ?", [idMedia], function (err) {
            if (err) {
                console.error("Error al marcar el medio como inactivo:", err);
                return res.redirect('/admin/Media/listar?message=Error al eliminar el medio');
            }
            console.log(`Medio con ID ${idMedia} marcado como inactivo.`);
            res.redirect('/admin/Media/listar?message=Medio eliminado exitosamente');
        });
    }
};

module.exports = MediaController;

